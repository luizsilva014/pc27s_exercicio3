/**
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * 
 * Autor: Luiz Alberto da Silva
 * Ultima modificacao: 09/08/2017
 */
package exemplo3;

import java.util.*;

public class ArrayWriter implements Runnable {

    private final VetorCompartilhado vetorCompartilhado;
    private int startValue; 
        
    public ArrayWriter(int value, VetorCompartilhado vetor){
        startValue=value;
        vetorCompartilhado=vetor;
    }//fim do construtor
    
    public void run(){
        synchronized(vetorCompartilhado){
            for (int i=startValue; i<startValue+3; i++){
                vetorCompartilhado.add(i); //Adiciona um elemento em ordem crescente no vetor compartilhado
                vetorCompartilhado.sub(i+10);
            }
        }
        

    }//fim run
    
}
